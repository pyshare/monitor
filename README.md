# monitor

Repository for pyshare monitor service.

## Prerequisite

- Docker
- Docker Compose

## How to setup

Ensure that pyshare services are running, and run following command:

```shell
docker-compose up -d
```
